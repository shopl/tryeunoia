
document.addEventListener('DOMContentLoaded', () => {


	var active_tab = document.querySelector('.tab-titles .tab-title:first-child');
    active_tab.classList.add("active");
    
	var active_category = active_tab.getAttribute("data-id");
    console.log(active_category);
	var category_name = '[data-category="' + active_category+'"]';
	var items = document.querySelectorAll(category_name);
	items.forEach(item => {
		item.classList.remove("hidden");
	});
	
	var tab_titles = document.getElementsByClassName("tab-title");

	var updateFaq = function() {
		var all_title = document.querySelectorAll('.tab-title');
		all_title.forEach(item => {
			item.classList.remove("active");
		});
		this.classList.add("active");
		var all_faq = document.querySelectorAll('.sf__custom-accordion')
		all_faq.forEach(item => {
			item.classList.add("hidden");
		});
		var active_category = this.getAttribute("data-id");
		var category_name = '[data-category="' + active_category+'"]';
		var items = document.querySelectorAll(category_name);
		items.forEach(item => {
			item.classList.remove("hidden");
		});
	};

	for (var i = 0; i < tab_titles.length; i++) {
		tab_titles[i].addEventListener('click', updateFaq, false);
	}
	
});